"Agnostiq Q: Quantum FHE (Almost) As Secure As Classical Quantum"
# This routine implements quantum one time pad 

import numpy as np
from qiskit import *
%matplotlib inline
import numpy as np 
import random
circ = QuantumCircuit(1)#test

#from qiskit import ClassicalRegister, QuantumRegister, QuantumCircuit, execute, Aer, quantum_info as qi
from math import cos, sin, pi


def QOTPKeygen(nqbits):# Sample two classical bits per qubits, first one x and second one y : 
    return np.random.choice([0,1], size= 2*nqbits)


def QOTPEnc(key,qbits): #Apply the Pauli transformation X^xZ^z on  φ, Encryption 
    
    if key[0]==1:
        qbits.x(0)
    if key[1]==1:
        qbits.z(0)
    return(qbits)


def QOTPDec(key,qbits): #Apply the Pauli transformation Z^zX^x on  φ, Encryption 
    if key[0]==1:
        qbits.z(0)
    if key[1]==1:
        qbits.x(0)
    return(qbits)

##Test##
key=QOTPKeygen(1)
a=QOTPEnc(key, circ)

a.draw('mpl')

#b=QOTPDec(key,a)
#b.draw()




